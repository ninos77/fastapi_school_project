from fastapi import FastAPI, HTTPException, Query
import psycopg
from typing import Optional, List

app = FastAPI()

@app.on_event("startup")
def startup():
    app.db = psycopg.connect("dbname=postgres user=postgres host=localhost password=testpass")

@app.on_event("shutdown")
def shutdown():
    app.db.close()


@app.get("/stores",tags=['Obligatorisk'])
def get_stores():
    """
    Denna funktion ska returnera data om stores (namn
    och fullständigt address).
    """
    with app.db.cursor() as cur:
        data = {"data": []}
        
        cur.execute("SELECT stores.name,address,zip,city FROM store_addresses INNER JOIN stores ON store_addresses.store=stores.id")
        for record in cur.fetchall():
            full_adress = record[1] + " " + record[3] + " " + record[2]
            data["data"].append({"name": record[0], "full address": full_adress })
        return data



@app.get("/stores/{name}",tags=['Obligatorisk'])
def get_stores(name):
    """
    Denna funktion returnerar data (namn och
    fullständigt address) för en specifik butik, vald via namn. Om ett namn
    som inte finns i DB anges, returnera 404 Not Found
    """
    with app.db.cursor() as cur:
        cur.execute("SELECT stores.name,address,zip,city FROM store_addresses \
        INNER JOIN stores ON store_addresses.store=stores.id  \
        WHERE name=%(name)s", {"name": name})

        row = cur.fetchone()

        if not row:
            raise HTTPException(status_code=404, detail="Name not found")
        else:
            data = {"data": []}
            full_adress = row[1] + " " + row[3] + " " + row[2]
            data["data"].append({"name": row[0], "full address": full_adress })
            return data

@app.get("/cities",tags=['Obligatorisk'])
def get_cities(zip: Optional[int] = None):
    """
    Denna endpoint returnerar alla unika städer där minst en butik finns.   
    Om query-parameter zip är given, den filtrerar med den och 
    visar bara städer med den specifika postkod.
    """
    with app.db.cursor() as cur:
        if zip:
            cur.execute("SELECT DISTINCT city FROM store_addresses WHERE zip=%(zip)s", {"zip": str(zip)})
        else:
            cur.execute("SELECT DISTINCT city FROM store_addresses")
        
        data = {"data": []}
        for row in cur:
            data["data"].append(row[0])

        return data



#========================================= FRIVILLIGT =========================================================0

@app.get("/cities/",tags=['Friviligt'])
def get_cities(zip: List[int] = Query(None)):
    """
    GET /cities query parameter zip ska kunna anges fler
    gånger, och då måste alla städer som stämmer överens med en zip vara
    med i resultatet som returneras.
    i.e.: GET /cities?zip=12345&zip=54321
    """
    with app.db.cursor() as cur:
        if zip:
            zip_str = [str(x) for x in zip]
            cur.execute(psycopg.sql.SQL("SELECT DISTINCT city FROM store_addresses WHERE zip = ANY({})").format([zip_str]))
        else:
            cur.execute("SELECT DISTINCT city FROM store_addresses")
        
        data = {"data": []}
        for row in cur:
            data["data"].append(row[0])

        return data



@app.get("/stores/",tags=['Friviligt'])
def get_stores(get_format: Optional[str] = None):
    """
    FRIVILLIGT: ska kunna ta emot en query parameter format som kan enbart vara en av följande värden:  
    - full: all info (namn + fullständigt address) returneras 
    - name: bara namn på stores returneras  
    - compact: bara namn och stad returneras 
    Om format anges med något annat värde än de 3 ovan, ett felmeddelande 
    ska returneras (status code: 422 Unprocessable Entry). Om format inte anges, anta full.
    """
    with app.db.cursor() as cur:
        data = {"data": []}

        if get_format == "full" or not get_format:
            cur.execute("SELECT stores.name,address,zip,city FROM store_addresses INNER JOIN stores ON store_addresses.store=stores.id")
            for record in cur.fetchall():
                full_adress = record[1] + " " + record[3] + " " + record[2]
                data["data"].append({"name": record[0], "full_adress": full_adress })
        elif get_format == "name":
            cur.execute("SELECT name FROM stores")
            for record in cur.fetchall(): 
                data["data"].append({"name": record[0]})
        elif get_format == "compact":
            cur.execute("SELECT stores.name,city FROM store_addresses INNER JOIN stores ON store_addresses.store=stores.id")
            for record in cur.fetchall():
                data["data"].append({"name": record[0], "city": record[1] })
        else:
            raise HTTPException(status_code=422, detail="Wrong format request")
        return data