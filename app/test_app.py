"""tests for app.py
"""
from fastapi.testclient import TestClient
import psycopg

from app import app
app.db = psycopg.connect("postgresql://postgres:testpass@localhost/postgres")

client = TestClient(app)

def test_get_stores():
    """This tests the response status code and details for a correct output to the
    /stores endpoint.

    """    
    response = client.get("/stores")
    assert response.status_code == 200
    assert response.json() == {
  "data": [
    {
      "name": "Djurjouren",
      "full address": "Upplandsgatan 99 Stockholm 12345"
    },
    {
      "name": "Djuristen",
      "full address": "Skånegatan 420 Falun 54321"
    },
    {
      "name": "Den Lilla Djurbutiken",
      "full address": "Nätverksgatan 22 Hudiksvall 55555"
    },
    {
      "name": "Den Stora Djurbutiken",
      "full address": "Routergatan 443 Hudiksvall 54545"
    },
    {
      "name": "Noahs Djur & Båtaffär",
      "full address": "Stallmansgatan 666 Gävle 96427"
    }
  ]
}

def test_get_store_by_name_ok():
    response = client.get("/stores/Djuristen")
    assert response.status_code == 200
    assert response.json() == {
  "data": [
    {
      "name": "Djuristen",
      "full address": "Skånegatan 420 Falun 54321"
    }
  ]
}
    
def test_get_store_not_found():
    response = client.get("/stores/zoo")
    assert response.status_code == 404
    assert response.json() == {"detail": "Name not found"}

def test_get_cities():
    response = client.get("/cities/")
    assert response.status_code == 200
    assert response.json() == {
  "data": [
    "Falun",
    "Gävle",
    "Stockholm",
    "Hudiksvall"
  ]
}


def test_get_cities_by_zip():
    response = client.get("/cities?zip=55555")
    assert response.status_code == 200
    assert response.json() == {
  "data": [
    "Hudiksvall"
  ]
}

def test_get_cities_wrong_zip():
    response = client.get("/cities?zip=02020")
    assert response.status_code == 200
    assert response.json() == {
  "data": []
}

    
#================FRIVILLIGT============================
def test_get_cities_list_by_zip():
    response = client.get("/cities/?zip=54321&zip=54545&zip=96427")
    assert response.status_code == 200
    assert response.json() == {
  "data": [
    "Falun",
    "Gävle",
    "Hudiksvall"
  ]
}


def test_get_store_by_full():
    response = client.get("/stores/?get_format=full")
    assert response.status_code == 200
    assert response.json() == {
  "data": [
    {
      "name": "Djurjouren",
      "full_adress": "Upplandsgatan 99 Stockholm 12345"
    },
    {
      "name": "Djuristen",
      "full_adress": "Skånegatan 420 Falun 54321"
    },
    {
      "name": "Den Lilla Djurbutiken",
      "full_adress": "Nätverksgatan 22 Hudiksvall 55555"
    },
    {
      "name": "Den Stora Djurbutiken",
      "full_adress": "Routergatan 443 Hudiksvall 54545"
    },
    {
      "name": "Noahs Djur & Båtaffär",
      "full_adress": "Stallmansgatan 666 Gävle 96427"
    }
  ]
}
    
def test_get_store_by_name():
    response = client.get("/stores/?get_format=name")
    assert response.status_code == 200
    assert response.json() == {
  "data": [
    {
      "name": "Djurjouren"
    },
    {
      "name": "Djuristen"
    },
    {
      "name": "Den Lilla Djurbutiken"
    },
    {
      "name": "Den Stora Djurbutiken"
    },
    {
      "name": "Noahs Djur & Båtaffär"
    }
  ]
}


def test_get_store_by_compact():
    response = client.get("/stores/?get_format=compact")
    assert response.status_code == 200
    assert response.json() == {
  "data": [
    {
      "name": "Djurjouren",
      "city": "Stockholm"
    },
    {
      "name": "Djuristen",
      "city": "Falun"
    },
    {
      "name": "Den Lilla Djurbutiken",
      "city": "Hudiksvall"
    },
    {
      "name": "Den Stora Djurbutiken",
      "city": "Hudiksvall"
    },
    {
      "name": "Noahs Djur & Båtaffär",
      "city": "Gävle"
    }
  ]
}
